/****** Object:  Table [dbo].[Config]    Script Date: 8/31/2020 6:35:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Config](
	[Id] [int] NOT NULL,
	[GameEnabled] [bit] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegisteredUser]    Script Date: 8/31/2020 6:35:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegisteredUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](500) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_RegisteredUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Config] ([Id], [GameEnabled], [DateUpdated]) VALUES (1, 1, CAST(N'2020-08-18T14:35:59.990' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[RegisteredUser] ON 
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (1, N'smallak@gmrmarketing.com
', CAST(N'2020-08-06T10:37:48.290' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (2, N'aschafer@gmrmarketing.com', CAST(N'2020-08-06T10:37:53.773' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (3, N'lmatovich@gmrmarketing.com', CAST(N'2020-08-06T10:37:59.510' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (4, N'mkhalil@gmrmarketing.com', CAST(N'2020-08-06T10:38:04.040' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (5, N'crisher@gmrmarketing.com', CAST(N'2020-08-06T10:38:08.507' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (6, N'marie.christoffersen@campari.com', CAST(N'2020-08-06T10:38:13.570' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (7, N'kfries@gmrmarketing.com', CAST(N'2020-08-06T10:51:16.880' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (8, N'smallak@gmrmarketing.com
', CAST(N'2020-08-06T11:57:51.460' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (9, N'smallak@gmail.com', CAST(N'2020-08-06T11:59:37.927' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (10, N'smallak@gmrmarketing.com', CAST(N'2020-08-06T12:00:09.643' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (11, N'marie.christoffersen@campari.com', CAST(N'2020-08-07T08:35:34.060' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (12, N'mariechristoffersen@campari.com', CAST(N'2020-08-07T08:36:45.370' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (13, N'mariechristoffersen@gmrmarketing.com', CAST(N'2020-08-07T08:36:56.497' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (14, N'mariec@gmrmarketing.com', CAST(N'2020-08-07T08:38:55.620' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (15, N'mkhalil@gmrmarketing.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (16, N'khalilmm16@uww.edu', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (17, N'kackerman@gmrmarketing.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (18, N'jsdewing@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (19, N'sylviroy@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (20, N'amanda.freestone@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (21, N'shakestirpour41@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (22, N'roxanni1380@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (23, N'zbtfarva@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (24, N'cweise01@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (25, N'mastro1354@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (26, N'lauren.wong@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (27, N'a.houghtaling22@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (28, N'katherinenoelparsons143@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (29, N'CarlosGarcia1994.cg@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (30, N'BeerBongForTheLady@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (31, N'lindsay.parsons@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (32, N'ladyf3rn@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (33, N'caldwellgroupintl@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (34, N'Garrett@EvilGeniusBeer.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (35, N'jlespo13@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (36, N'perry.joshua1@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (37, N'DrinkingWithDarian@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (38, N'dana.detoro@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (39, N'sam@thewhiskeysomm.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (40, N'buddy@thejunglebird.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (41, N'vadim.mrv@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (42, N'fueledbyagave@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (43, N'trishrenehan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (44, N'benvodka@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (45, N'rosey@nyu.edu', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (46, N'joseph@bcr8ive.org', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (47, N'mmanago@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (48, N'Kenneth@deviliciouseatery.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (49, N'sylviam05@msn.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (50, N'regan.okon@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (51, N'sebastian.tollius@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (52, N'blancapsanchez@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (53, N'natalie.g.smythe@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (54, N'charleshsteadman@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (55, N'mkatekel@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (56, N'soundguyjon@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (57, N'tia@tulipsftw.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (58, N'alexbarbatsis@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (59, N'dearzona@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (60, N'rob@amazingworldoffood.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (61, N'roxana.medina07@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (62, N'sgorelick@colangelopr.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (63, N'brittanyychavez@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (64, N'vidiot@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (65, N'bernadette.knight@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (66, N'megan.usefara@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (67, N'themarcsauve@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (68, N'erik_puryear@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (69, N'alishazaveri55@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (70, N'ntnchelof@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (71, N'rebeccaleighsil@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (72, N'djajax@aol.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (73, N'alan.sanchez92@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (74, N'craigbestjr@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (75, N'yasmin.quiles@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (76, N'jeff@freeagentmedia.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (77, N'Coolvibes86@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (78, N'grayandre85@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (79, N'reeseep5@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (80, N'Mellenthal@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (81, N'Dsdsyogi@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (82, N'woody.latour37@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (83, N'val.hernandez99@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (84, N'spain.james.spain@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (85, N'brian.m.nixon@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (86, N'czingler3@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (87, N'jessrehs.chs@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (88, N'brendaneger@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (89, N'rrpines3383@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (90, N'bibarra@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (91, N'stefan@dotdotdotcharlotte.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (92, N'lingozoom@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (93, N'mauricioromo98@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (94, N'johnfry.johnfry@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (95, N'dmeveritt1@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (96, N'meganpaige@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (97, N'gtapia88@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (98, N'pmanchina@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (99, N'galvan366@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (100, N'LauraReidy@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (101, N'oceancrawford@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (102, N'Emilia_Harband@tpnretail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (103, N'smgsharp@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (104, N'rendon.cesar9@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (105, N'prillamanml@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (106, N'e4rodrig@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (107, N'christina@bubblesandagave.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (108, N'frasco.joe@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (109, N'marell4.chi@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (110, N'saraellenburnett@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (111, N'BordenOrnelaz@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (112, N'abby@station26brewing.co', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (113, N'dinorawr@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (114, N'rachel.markoja@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (115, N'davidlam31@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (116, N'capturingcocktails@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (117, N'dawnofthefleming@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (118, N'angiemwebb91@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (119, N'mrwagner72983@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (120, N'kkgcalma@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (121, N'simon@endlesswest.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (122, N'sewele@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (123, N'personalstyleworks@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (124, N'makeda.gebre@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (125, N'cassie.artishenko@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (126, N'Micah.r.graham@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (127, N'm.goff.520@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (128, N'joshuasilverstein.js@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (129, N'jerommorris@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (130, N'tpreslan@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (131, N'tristyn@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (132, N'renteriasamantha18@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (133, N'jamesdefrance@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (134, N'devilmademedoit@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (135, N'darwin.pornel@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (136, N'bennyzapien@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (137, N'jjvargasbastidas@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (138, N'warner.perkins@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (139, N'chrisgrossman@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (140, N'roxzannd@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (141, N'mark.soifer@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (142, N'Mlsoifer@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (143, N'info@threelupsofsuar.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (144, N'chimene.montgomery@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (145, N'athena.a.menis@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (146, N'berdecia@colectivoicaro.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (147, N'ken.siukuen.lin@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (148, N'mroe16@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (149, N'tarafdavies@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (150, N'ch.cocktails@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (151, N'amberrose0402@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (152, N'rhea.s.buck@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (153, N'kfaheyy@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (154, N'Johnbvagabond@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (155, N'nick@moverandshakerco.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (156, N'l.soto93@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (157, N'clairetimestwo@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (158, N'chris.ludemann-davis@redbull.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (159, N'RumAndKoky@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (160, N'jjjamurillo@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (161, N'seeemilywrite@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (162, N'nnicholas4@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (163, N'avalosmarco43@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (164, N'richard.j.nino@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (165, N'nathandokuley@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (166, N'Nasthia861@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (167, N'wann21@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (168, N'benanderson@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (169, N'michael.laureano@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (170, N'cmratx13@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (171, N'emillio@503w.co', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (172, N'evancharest2011@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (173, N'ashleymiller@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (174, N'shelley.mayen@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (175, N'nickammazzalorso@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (176, N'sarahmwilderman@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (177, N'clintdempsey@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (178, N'cleeb@outlook.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (179, N'lesliecainpiano@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (180, N'cameron.david.winkelman@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (181, N'ccusac31@gmail.con', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (182, N'michaelwester@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (183, N'csssouther@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (184, N'jkim.indiana@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (185, N'heathgosselinmma@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (186, N'gsciore@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (187, N'ty.hang@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (188, N'dmz75@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (189, N'estrellagarcia95@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (190, N'mjsilvagaete@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (191, N'nicoledesmond@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (192, N'johnzamorajr@me.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (193, N'LissaBrennan.lb@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (194, N'kerry@potluckbeverage.co', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (195, N'angela.sanks@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (196, N'JoshuaLenardos@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (197, N'claire@cbllc.me', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (198, N'nainow@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (199, N'ebonyrose99@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (200, N'mxlgst@dslextreme.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (201, N'austin.baumann@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (202, N'jmcclellan724@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (203, N'bryanxlyrical@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (204, N'damienburton44@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (205, N'jamesmontgomery00@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (206, N'lauren.sepe@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (207, N'adrianasoley@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (208, N'jemmwork@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (209, N'chelle600@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (210, N'fantasy.anjel@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (211, N'dgledwards@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (212, N'aejohnston2011irc@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (213, N'riglesias@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (214, N'cocoxtini@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (215, N'maliarno23@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (216, N'pacoang21@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (217, N'angela.vieux@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (218, N'bootslof@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (219, N'davidyett@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (220, N'ddanaila@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (221, N'sjbaculi@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (222, N'chicas.tito@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (223, N'Swaldaias@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (224, N'carlitasway70@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (225, N'jgonzalez1116@msn.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (226, N'lchirivi@me.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (227, N'kassandrateresa@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (228, N'missykissling@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (229, N'snapgunna@aol.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (230, N'tonybaker@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (231, N'thrishany@aol.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (232, N'leah.miller@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (233, N'bjart7793@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (234, N'arleen.delima@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (235, N'jstargillespie@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (236, N'greenlawad@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (237, N'raymond.reynaga@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (238, N'esmeralda.lopez@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (239, N'rogerjamesphoto@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (240, N'mplabrie@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (241, N'patrick.l.todd@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (242, N'Rachel.Harrison@lalcomm.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (243, N'photography1@me.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (244, N'lindsayclem@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (245, N'Lbelnavis@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (246, N'montsefranco@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (247, N'jontostin@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (248, N'b.whitaker2@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (249, N'jimpipes@swbell.net', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (250, N'lselkowitz@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (251, N'jamesslater66@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (252, N'Laura.Scoggin@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (253, N'gmart1213@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (254, N'sjomartin314@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (255, N'lukasgarciao@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (256, N'pigliquorsoink@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (257, N'undergroundcookingclub@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (258, N'aliceeetang@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (259, N'jcladybug@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (260, N'monicalynnraymond@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (261, N'laraeperrin90@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (262, N'rapavy@mail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (263, N'mmaxwell@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (264, N'iancharleshanrahan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (265, N'emichalovic@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (266, N'msmimiburnham@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (267, N'Marys44x@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (268, N'avilezkiki@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (269, N'reid@lasalmasrotas.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (270, N'deepanktuli@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (271, N'kramerjensenj@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (272, N'atrapani@berkeley.edu', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (273, N'morganephriam@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (274, N'mouze1210@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (275, N'cephas.cho@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (276, N'melzorbar@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (277, N'marcot1285@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (278, N'sarahmccoy007@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (279, N'jlgross05@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (280, N'jmorales.edu@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (281, N'pdxcigars@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (282, N'antoniomeza1988@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (283, N'kcockran117@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (284, N'tovakamila@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (285, N'arhartmam@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (286, N'den@extradark.org', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (287, N'clinthis@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (288, N'thecheykinch@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (289, N'treyna31@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (290, N'milesjkerr@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (291, N'frank@craftspiritsfest.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (292, N'bwilsonbtd@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (293, N'chefreggiegold@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (294, N'dlynpierre@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (295, N'thehealthybartender@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (296, N'anthony.kolodziej@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (297, N'ethanburton94@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (298, N'chris4wine@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (299, N'mhappy8@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (300, N'schafer.purdue@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (301, N'jordan.mcdonald@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (302, N'Gregoryschutt@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (303, N'robert.rau@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (304, N'kristin@ofwineandwhiskey.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (305, N'taylor.breit@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (306, N'dvance@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (307, N'nicholas.s.rowland@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (308, N'wtrachta@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (309, N'Shawnewilliams75@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (310, N'jeng731@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (311, N'lkrall91@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (312, N'pierre.voltaire2.0@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (313, N'eventswithjess@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (314, N'danny.moch@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (315, N'peter.haubenschild@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (316, N'julesgmz7@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (317, N'JerrieP71@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (318, N'aaronkolitz@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (319, N'gabriela.villafana@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (320, N'towery.candace@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (321, N'regan@leespirits.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (322, N'michhelmke@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (323, N'jamesthornton@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (324, N'maddy@poppytooker.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (325, N'jlcherriz@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (326, N'carincastillo@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (327, N'dkelley@clevelandave.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (328, N'priscillamariebartendint@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (329, N'sandy.perez@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (330, N'andrew@barcademy.co', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (331, N'peter.vanschoick@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (332, N'rashaundhall@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (333, N'barbabeofbrooklyn@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (334, N'bradgoocher@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (335, N'sharonrboone@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (336, N'robyndwebb@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (337, N'stephanie@thesocialny.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (338, N'sschaffer@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (339, N'dklugie@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (340, N'plstarlight@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (341, N'bobbydag@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (342, N'rachelwasserman@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (343, N'michaeljrholiday@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (344, N'Jill.Montgomery@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (345, N'msmegmary@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (346, N'timmymack@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (347, N'iugalde2010@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (348, N'lisa@propermagnolia.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (349, N'adamgarzona@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (350, N'arnette.jacquenette@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (351, N'cheers@cocktailsnobnyc.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (352, N'lethamaldonado3@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (353, N'robertmaurer@southernwine.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (354, N'drinkinginsa@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (355, N'adhamlyles@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (356, N'cschleyer@worcester.edu', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (357, N'alexbarrancotampa@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (358, N'tj.green@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (359, N'pdxbittersproject@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (360, N'myles@thesocialny.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (361, N'blair@blairbeavers.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (362, N'zajocramer@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (363, N'cannestra_joe@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (364, N'richard.murguia@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (365, N'jrios1461@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (366, N'candy.shaw@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (367, N'pingodoche@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (368, N'guerra.luisa@ymail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (369, N'ddma1164@aol.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (370, N'carla.e.gilfillan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (371, N'lvlatour@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (372, N'fsgon@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (373, N'lstarrplatt@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (374, N'gaydaze64@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (375, N'liquidmobility@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (376, N'TaylorMaidecl@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (377, N'jeff.hagley@natdistco.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (378, N'danadestefano@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (379, N'willmosier@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (380, N'paige.pomerenke@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (381, N'lashawnthebartender@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (382, N'priscillamariebartending@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (383, N'hamilldn@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (384, N'mj_carr@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (385, N'AlexA2336@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (386, N'marvinholiday88@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (387, N'theculturalattache@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (388, N'ezingler1@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (389, N'lincolnwrites@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (390, N'laurablindsay@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (391, N'flemma67@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (392, N'justin@oddduckaustin.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (393, N'sakejoy@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (394, N'rebecca@lodgeroomhlp.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (395, N'arlsmith12@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (396, N'jamesgribble@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (397, N'kaleebaldwin1147@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (398, N'lapescadora562@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (399, N'melissa3882@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (400, N'algee76@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (401, N'mechristoffersen@Gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (402, N'dan.butkus@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (403, N'grundleger.a@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (404, N'andrea.baptista@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (405, N'rwcocktailqueen@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (406, N'closingtimecocktails@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (407, N'tommyd22@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (408, N'rogherjeri@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (409, N'francesca.stockert@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (410, N'nataliecarlo@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (411, N'flanagan.joy@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (412, N'leahthemoss@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (413, N'dabramowitz@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (414, N'jacob.rodriguez@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (415, N'dofernandez77@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (416, N'tarasaltz@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (417, N'bbedford05@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (418, N'justineann1430@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (419, N'cody.miller@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (420, N'richard.balzaretti@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (421, N'bmcfarlanetech@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (422, N'josiahjamesfrancis@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (423, N'rick@mercbaraz.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (424, N'carrillo86luis@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (425, N'rachellane931@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (426, N'mashmist@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (427, N'csemp@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (428, N'paulalukas27@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (429, N'coachbushby@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (430, N'tom.himberger@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (431, N'josephineliz91@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (432, N'babysharkjoz@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (433, N'eharrison09@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (434, N'cpaek@escoretail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (435, N'missjackiedl10@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (436, N'benjamin.zorn1987@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (437, N'John@biercaves.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (438, N'brian.winget@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (439, N'marco.benson@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (440, N'lindsay.colford@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (441, N'knpepple@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (442, N'kclcortez@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (443, N'Jeremiah.Batucan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (444, N'clair.mclafferty@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (445, N'martzmeghan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (446, N'tdelpino25@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (447, N'asiegel@160over90.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (448, N'apoin@160over90.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (449, N'mintygirl@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (450, N'alissa.thompson2@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (451, N'naominguyen.ho@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (452, N'n_gandulla@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (453, N'LiquidFlair@live.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (454, N'gregory.salmen@va.gov', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (455, N'jpklock@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (456, N'wilkerson.a@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (457, N'bitterbotwin@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (458, N'drs3304@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (459, N'nicole@vodkagirlatx.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (460, N'shonmpkelley@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (461, N'k.rene.solomon@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (462, N'spencer.monroe@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (463, N'mmccarthy@colangelopr.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (464, N'twin655@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (465, N'peaceloveandfrankieness@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (466, N'crichards@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (467, N'tashalovescake@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (468, N'melodyschumacherj@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (469, N'christy.sabol@legacymarketing.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (470, N'dberliner1@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (471, N'cicrowley62787@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (472, N'kristen.albrecht@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (473, N'russ.marshalek@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (474, N'Jshukitt@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (475, N'jakobkramerjensen@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (476, N'kelly.pfeiffer@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (477, N'ratapan63@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (478, N'kristina@mrblack.co', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (479, N'jbarlass@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (480, N'april.alejandro@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (481, N'hoodb@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (482, N'deedledumpling@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (483, N'jarredcraven@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (484, N'malm.rachel@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (485, N'joshkruse414@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (486, N'mvurquizo@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (487, N'sean@cottonandcopperaz.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (488, N'lenora.weimorts@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (489, N'michael.r.carbajal@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (490, N'Jonathan.escucha@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (491, N'ccusac31@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (492, N'gndayri@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (493, N'arosenobrega@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (494, N'stevenknowswhiskey@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (495, N'Crystal.c.welch@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (496, N'jayman88s@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (497, N'catherinelien@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (498, N'dpt206@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (499, N'tlakeotes@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (500, N'jwil44@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (501, N'sickalone@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (502, N'Czelidon@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (503, N'laurenlizlee13@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (504, N'kaitlinfellers@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (505, N'angielst@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (506, N'meredith@homebrewedevents.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (507, N'tsoles@usbg.org', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (508, N'jim.rufo@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (509, N'samsaysmiami@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (510, N'lmatovich@gmrmarketing.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (511, N'ihowden@gmrmarketing.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (512, N'caitlinhooker810@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (513, N'stevacasey@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (514, N'mcmoberly@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (515, N'1raquel.feurtado@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (516, N'caitlink@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (517, N'theskatomasochist36c@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (518, N'felix.r.allen@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (519, N'Syljeter@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (520, N'michael.d.guzman@me.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (521, N'meganerickerson@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (522, N'Smdank5481@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (523, N'wes.holloway614@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (524, N'jayandreozzi@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (525, N'mylesgrene@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (526, N'michelleclifford702@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (527, N'dwiseoneaz@msn.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (528, N'Anjali@partender.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (529, N'ncbczeke@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (530, N'jayrivphoto@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (531, N'JamesMinhTran@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (532, N'oebeall@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (533, N'vroraus@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (534, N'ninamonemusic@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (535, N'espolon@daanon.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (536, N'quinn.ross@redbull.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (537, N'pilsenjla71@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (538, N'Jacobmedina15@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (539, N'Logan@SpiritedDiscussions.net', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (540, N'fsleeper@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (541, N'THESOMMELIER@GMAIL.COM', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (542, N'tamara.rose.usbg@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (543, N'olivia.ceiro@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (544, N'johncranmore@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (545, N'jeanelle.sims@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (546, N'linda.jockers@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (547, N'seijinanbu@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (548, N'trysaroj4357@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (549, N'dalvaradohouston@aol.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (550, N'jmsmith1011@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (551, N'whiskeybrittany@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (552, N'Lorenacastro1231@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (553, N'annie@deathorglorybar.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (554, N'agueri01@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (555, N'fyffeml@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (556, N'kelly.waters@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (557, N'adonnelly711@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (558, N'alex.sf.125@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (559, N'kevin.hoagland@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (560, N'paulspacekim@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (561, N'mindy.hansman@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (562, N'andrewjpellegrino@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (563, N'robyn.e.cohen@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (564, N'culinarylibations@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (565, N'michael.j.hoversten@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (566, N'kp@thearmorybk.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (567, N'leighannheidelberg@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (568, N'woodyapproved@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (569, N'Jdstanyard@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (570, N'adrienne.crumpton@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (571, N'sheenaodum@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (572, N'david.f.klemt@gmail.come', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (573, N'fatimareltub@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (574, N'eliebelony@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (575, N'joewhyte716@aim.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (576, N'cmannixsf@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (577, N'justin.frumkes@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (578, N'charityfalm0928@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (579, N'mmartz@160over90.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (580, N'chardmo@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (581, N'carl@whitechapelsf.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (582, N'jlangan84@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (583, N'jessieyoskin@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (584, N'ryanhoffman77@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (585, N'walkerfreida@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (586, N'jimrufo@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (587, N'kim@biga.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (588, N'ccd3511@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (589, N'Amanda.Schaffner@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (590, N'arturo.ayala@pcc.edu', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (591, N'lianaepavon@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (592, N'GRANTCOOTE@GMAIL.COM', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (593, N'jonmadrigal@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (594, N'dunavancj@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (595, N'mannymezcal09@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (596, N'REGINABR777@gmail.COM', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (597, N'juliocepeda25@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (598, N'david.miles.perricone@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (599, N'Greg@catchtwentyseven.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (600, N'danmurphy123@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (601, N'mdrum89021@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (602, N'victor070988@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (603, N'damiianbarmix@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (604, N'damiianh@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (605, N'toronamichele@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (606, N'tristan@residentdtla.con', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (607, N'isleyquinzelle@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (608, N'daniel@texaslifestylemag.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (609, N'jasonlatham13@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (610, N'drunkbuilder@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (611, N'peacockthebartender@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (612, N'miss.aubreemiller@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (613, N'aross0321@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (614, N'celisseberumen@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (615, N'bartamder@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (616, N'maggie.digiovanni@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (617, N'ch.dunsmoor@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (618, N'luisestrada1191@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (619, N'solomonkthomas@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (620, N'kernrodriguez.79@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (621, N'doug.twining@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (622, N'houstonsangelamorgan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (623, N'frank.dudley@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (624, N'brittnyridesherbike@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (625, N'sheilad92@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (626, N'mclainhedges@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (627, N'mmwilliams2010@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (628, N'anthonypabst@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (629, N'dominique127@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (630, N'cbanks.nj@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (631, N'liquidta2@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (632, N'caitlinhoop@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (633, N'fjsosa3@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (634, N'adolfomartinez3@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (635, N'maritzarocha@blackbottleent.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (636, N'benjaminjeffers1985@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (637, N'danielnuebler@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (638, N'handyasfoil@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (639, N'c.caitlynn0531@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (640, N'brendan.markham@redbull.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (641, N'lukezins@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (642, N'danitaylan@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (643, N'rabbitsd62@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (644, N'pattie.rost@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (645, N'michaelnank@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (646, N'Pacman006900@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (647, N'Bruins607@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (648, N'alliebenbrook@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (649, N'edwardhansel@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (650, N'amv.smith@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (651, N'david_osher@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (652, N'sincitynevada@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (653, N'andiehalaburda@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (654, N'ocerio@icloud.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (655, N'Jlopez5731@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (656, N'gregory@bootleggregcocktail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (657, N'bernardoconnellv@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (658, N'franz.gerhardter@redbull.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (659, N'jayfrench5@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (660, N'cararoadarmel@yahoo.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (661, N'desireev@outlook.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (662, N'ian@armoredbarkeep.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (663, N'robert.tornese@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (664, N'frenchassassin@me.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (665, N'amanda.dawn@myself.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (666, N'christamonster@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (667, N'playsteen@aol.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (668, N'gary.dritschler@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (669, N'aimeecoldren@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (670, N'james.child12@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (671, N'djsuan.sk@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (672, N'tim12stevens@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (673, N'jarred730@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (674, N'alexclausen@sgws.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (675, N'clsmith17@hotmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (676, N'jason.paradis@marriott.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (677, N'jessica.coplin@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (678, N'josh.seaburg@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (679, N'rolando.pettigrew@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (680, N'alextalfonso@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (681, N'wolfheadedconjuror@gmail.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (682, N'AWAA@comcast.net', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (683, N'steve.chasen@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (684, N'Karlyn.monroe@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (685, N'rachel.wasserman@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (686, N'jessamine.mclellan@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (687, N'Edward.hansel_ext@campari.com', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (688, N'crisher@gmrmarketing.com ', CAST(N'2020-08-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (689, N'emily@fiveoclockcreative.com', CAST(N'2020-08-24T11:28:30.497' AS DateTime))
GO
INSERT [dbo].[RegisteredUser] ([Id], [Email], [DateAdded]) VALUES (690, N'slade@970design.com', CAST(N'2020-08-24T16:22:46.210' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[RegisteredUser] OFF
GO
ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_GameEnabled]  DEFAULT ((0)) FOR [GameEnabled]
GO
ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
ALTER TABLE [dbo].[RegisteredUser] ADD  CONSTRAINT [DF_RegisteredUsers_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
