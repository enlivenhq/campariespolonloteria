﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CampariEspolonLoteria.Models.ViewModels
{
    public class LoginViewModel
    {
        public string AccessCode{ get; set;  }
        public string ErrorMessage { get; set; }

    }
}