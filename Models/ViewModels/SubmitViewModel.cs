﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CampariEspolonLoteria.Models.ViewModels
{
    public class SubmitViewModel
    {
        public bool RegistrationRequired { get; set; }
        public string Email { get; set;  }
        public int Month { get; set;  }
        public int Day { get; set; }
        public int Year { get; set; }
        public string ErrorMessage { get; set; }

    }
}