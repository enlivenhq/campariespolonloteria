﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CampariEspolonLoteria.Models.ViewModels
{
    public class ControlViewModel
    {
        public bool GameEnabled { get; set;  }
        public bool CAGameEnabled { get; set; }
        public bool RegistrationEnabled { get; set; }
        public bool CARegistrationEnabled { get; set; }

    }
}