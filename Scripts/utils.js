var sessionCards = JSON.parse(localStorage.getItem("cards") || "[]");

var utils = {
    init: function() {
        utils._getBoard();
        utils._cardClick();
        utils._startNewGame();
    },
    _getBoard: function () {
        //var url = "http://localhost:64129/Scripts/gameboards.json";
        var url = "/Scripts/gameboards.json";
        var data = {};

        $.ajax({
            type: 'GET',
            url: url,
            dataType: "json",
            data: data,
            success: function (data) {
                //console.log("SUCCESS", data);
                utils._displayBoard(data);
            },
            error: function () {
                //console.log("JSON call failed");
            }
        });
    },
    _displayBoard: function(data) {
        var d = data;
        var idx = Math.floor(Math.random() * 25) + 0;
        var boardNumber = document.querySelectorAll('.gameboard-number');
        var boardId = localStorage.getItem('boardId');
        var gameCard = document.querySelectorAll('.game-card');
        var card = document.querySelectorAll('.game-card-front');
        var printBtn = document.querySelector('[data-print-board]');
        var cardImg, gameboard;

        if (boardId !== null) {
            gameboard = d.gameboard[boardId];

            //printBtn.href = "http://localhost:64129/pdfs/gameboard_" + gameboard.id + ".pdf";
            printBtn.href = "/pdfs/gameboard_" + gameboard.id + ".pdf";

            for (var i = 0; i < 16; i++) {
                cardImg = new Image();
                cardImg.src = "/Images/cards/card_" + gameboard.cardNumbers[i] + ".png";
                card[i].appendChild(cardImg);
            }

            for (var j = 0; j < sessionCards.length; j++) {
                var savedCard = sessionCards[j];
                gameCard[savedCard].classList.add('--selected');
            }

            for (var k = 0; k < boardNumber.length; k++) {
                boardNumber[k].innerHTML = gameboard.id;
            }
           
        } else {
            gameboard = d.gameboard[idx];

            //printBtn.href = "http://localhost:64129/pdfs/gameboard_" + gameboard.id + ".pdf";
            printBtn.href = "/pdfs/gameboard_" + gameboard.id + ".pdf";

            for (var i = 0; i < 16; i++) {
               cardImg = new Image();
                cardImg.src = "/Images/cards/card_" + gameboard.cardNumbers[i] + ".png";
                card[i].appendChild(cardImg);
            }

            for (var j = 0; j < boardNumber.length; j++) {
                boardNumber[j].innerHTML = gameboard.id;
            }

            localStorage.setItem("boardId", gameboard.id);
        }
    },
    _cardClick: function() {
        var gameCard = document.querySelectorAll('.game-card');

        for (var i = 0; i < gameCard.length; i++) {
            gameCard[i].addEventListener("click", function() {
                this.classList.toggle('--selected');
                utils._addRemoveCards(this);
            }, false);
        }
    },
    _addRemoveCards: function(card) {
        var id = card.getAttribute('data-card-id');

        if (sessionCards.includes(id)) {
            sessionCards = utils._removeCard(sessionCards, id);
            localStorage.setItem("cards", JSON.stringify(sessionCards));
        } else {
            sessionCards.push(id);
            localStorage.setItem("cards", JSON.stringify(sessionCards));
        }
    },
    _removeCard: function(arr, value) {
        return arr.filter(function(ele){ return ele != value; });
    },
    _startNewGame: function() {
        var newGameBtn = document.querySelectorAll('[data-new-game]');
        var startGame = document.querySelector('[data-start-new]');
        var closeBtn = document.querySelectorAll('[data-close-modal]');
        var modal = document.querySelector('.overlay');

        for (var i = 0; i < closeBtn.length; i++) {
            newGameBtn[i].addEventListener('click', function() {
                modal.classList.toggle('--isVisible');
            }, false);

            closeBtn[i].addEventListener('click', function() {
                modal.classList.toggle('--isVisible');
            }, false);
        }

        startGame.addEventListener('click', function() {
            localStorage.removeItem("boardId");
            localStorage.removeItem("cards");
            location.reload();
        }, false);
    },
    //_checkAge: function () {
    //    var form = document.getElementById('form1')
    //    var now = Date.now(),
    //        x = document.getElementById("ageCheck"),
    //        bDay = "",
    //        start,
    //        elapsed,
    //        age;

    //    for (i = 0; i < x.length; i++) {
    //        bDay += x.elements[i].value + ",";
    //    }

    //    start = new Date(bDay);

    //    elapsed = now - start;
    //    age = Math.floor(((((elapsed / 1000) / 60) / 60) / 24) / 365);
        
    //    if (age < 21) {
    //        console.log("AGE", age);
    //    } else {
    //        form.submit();
    //    }

    //},
    //_browserClose: function () {
    //    window.onbeforeunload = function () {
    //        localStorage.clear();
    //        return '';
    //    };
    //}
}