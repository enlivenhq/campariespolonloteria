﻿using CampariEspolonLoteria.Models.ViewModels;
using CampariEspolonLoteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CampariEspolonLoteria.Controllers
{
    public class HomeController : Controller
    {
        private CampariEspolonLoteriaEntities _db = new CampariEspolonLoteriaEntities();
        public ActionResult Index()
        {
            var config = _db.Configs.First();
            var host = Request.Url.Host;
            var isCA = host.EndsWith(".ca");

            if (!isCA && !config.GameEnabled)
                return RedirectToAction("ComingSoon");

            if (isCA && !config.CAGameEnabled)
                return RedirectToAction("ComingSoon");

            return View(new SubmitViewModel()
            {
                RegistrationRequired = isCA ? config.CARegistrationEnabled : config.RegistrationEnabled
            });
        }

        [HttpPost]
        public ActionResult Index(SubmitViewModel submission)
        {
            var config = _db.Configs.First();
            var host = Request.Url.Host;
            var isCA = host.EndsWith(".ca");
            var registrationRequired = isCA ? config.CARegistrationEnabled : config.RegistrationEnabled;
            submission.RegistrationRequired = registrationRequired;

            if (registrationRequired)
            {
                var user = _db.RegisteredUsers.FirstOrDefault(x => x.Email == submission.Email);
                if (user == null)
                {
                    //invalid user
                    submission.ErrorMessage = "Sorry, we don't see that email! Please ensure you are using the email you used for registration.";
                    return View(submission);
                }

                FormsAuthentication.SetAuthCookie(submission.Email, false);
            }

            if (submission.Year == 0 || submission.Day == 0 || submission.Month == 0)
            {
                submission.ErrorMessage = "Please enter a valid date of birth.";
                return View(submission);
            }
            var dob = new DateTime(submission.Year, submission.Month, submission.Day);
                        
            if (dob.AddYears(isCA ? 19 : 21) > DateTime.Now)
            {
                //under legal age
                submission.ErrorMessage = "Sorry, you must be of legal age to access this site.";
                return View(submission);
            }

            return RedirectToAction("Game");
        }

        public ActionResult ComingSoon()
        {

            return View();
        }


        public ActionResult Game()
        {


            var config = _db.Configs.First();
            var host = Request.Url.Host;
            var isCA = host.EndsWith(".ca");

            if (!isCA && !config.GameEnabled)
                return RedirectToAction("ComingSoon");

            if (isCA && !config.CAGameEnabled)
                return RedirectToAction("ComingSoon");

            var registrationRequired = isCA ? config.CARegistrationEnabled : config.RegistrationEnabled;

            if (registrationRequired)
            {
                bool valid = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
                if (!valid)
                    return RedirectToAction("Index");
            }

            ViewBag.Message = "Your game page.";

            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Your login page.";

            return View();
        }

    }
}