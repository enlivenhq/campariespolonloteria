﻿using CampariEspolonLoteria.Models;
using CampariEspolonLoteria.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace CampariEspolonLoteria.Controllers
{
    
    public class AdminController : Controller
    {
        private CampariEspolonLoteriaEntities _db = new CampariEspolonLoteriaEntities();

        // GET: Admin
        public ActionResult Index()
        {
            return View(new LoginViewModel());
        }
        // GET: Admin
        [HttpPost]
        public ActionResult Index(LoginViewModel login)
        {
            if (FormsAuthentication.Authenticate("admin", login.AccessCode))
            {
                FormsAuthentication.SetAuthCookie("admin", true);
                return RedirectToAction("Control");
            }
            else
            {
                login.ErrorMessage = "Invalid access code.";
            }
            return View(login);
        }

        [Authorize(Users = "admin")]
        [Authorize]
        public ActionResult Control()
        {
            var config = _db.Configs.First();
            var control = new ControlViewModel
            {
                GameEnabled = config.GameEnabled,
                CAGameEnabled = config.CAGameEnabled,
                RegistrationEnabled = config.RegistrationEnabled,
                CARegistrationEnabled = config.CARegistrationEnabled    
            };
            return View(control);
        }

        [Authorize(Users = "admin")]
        [HttpPost]
        public ActionResult Control(ControlViewModel control)
        {
            var config = _db.Configs.First();
            config.GameEnabled = control.GameEnabled;
            config.CAGameEnabled = control.CAGameEnabled;
            config.RegistrationEnabled = control.RegistrationEnabled;
            config.CARegistrationEnabled = control.CARegistrationEnabled;
            config.DateUpdated = DateTime.Now;
            _db.SaveChanges();
            return View(control);
        }
    }
}